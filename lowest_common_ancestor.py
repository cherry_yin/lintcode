class TreeNode():
	def __init__(self, val):
		self.left = None
		self.right = None
		self.val = val

import copy
class Solution:
    """
    @param root: The root of the binary search tree.
    @param A and B: two nodes in a Binary.
    @return: Return the least common ancestor(LCA) of the two nodes.
    """ 
    def lowestCommonAncestor(self, root, A, B):
        if (root is None) or (root == A) or (root == B):
        	return root

        # Divide
        left = self.lowestCommonAncestor(root.left, A, B)
        right = self.lowestCommonAncestor(root.right, A, B)

        # Conquer
        if (left is not None) and (right is not None):
        	return root
        elif left is not None:
        	return left
        elif right is not None:
        	return right
        else:
        	return None


if __name__ == "__main__":
	r1 = TreeNode(4)
	r1.left = TreeNode(3)
	r1.right = TreeNode(7)
	r1.right.left = TreeNode(5)
	r1.right.right = TreeNode(6)

	print Solution().lowestCommonAncestor(r1, r1, r1.right.left).val