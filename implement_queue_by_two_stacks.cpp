#include <iostream>
#include <stack>
using namespace std;

class Queue {
public:
    stack<int> stack1;
    stack<int> stack2;

    Queue()
    {
        // do intialization if necessary
    }

    void push(int element)
    {
        stack1.push(element);
    }
    
    int pop()
    {
        if(stack2.empty())
        {
            stack1ToStack2();
        }
        int top_value = stack2.top();
        stack2.pop();
        return top_value;
    }

    int top()
    {
        if(stack2.empty())
        {
            stack1ToStack2();
        }
        return stack2.top();
    }

private:
    void stack1ToStack2()
    {
        while(!stack1.empty())
        {
            stack2.push(stack1.top());
            stack1.pop();
        }
    }

};
