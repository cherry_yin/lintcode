#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    /**
     * @param nums an integer array
     * @param target an integer
     * @return the difference between the sum and the target
     */
    int twoSumCloset(vector<int>& nums, int target) {
        sort(nums.begin(), nums.end());
        int i = 0;
        int j = nums.size() - 1;
        
        int min_diff = 999999999;
        int diff;
        
        while(i < j)
        {
            if(nums[i] + nums[j] <= target)
            {
                diff = target - nums[i] - nums[j];
                if(diff < min_diff)
                {
                    min_diff = diff;
                }
                i++;
            }
            else if(nums[i] + nums[j] > target)
            {
                diff = nums[i] + nums[j] - target;
                if(diff < min_diff)
                {
                    min_diff = diff;
                }
                j--;
            }
        }
        return min_diff;
    }
};

int main(void)
{
	int n[] = {0, 1, 2};
	vector<int> vec_n(n, n + sizeof(n) / sizeof(int));
	int target = 0;

	Solution ss;
	cout << ss.twoSumCloset(vec_n, target) << endl;

	return 0;
}