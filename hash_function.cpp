#include <iostream>
#include <string>

using namespace std;

//note: 这道题直接算会溢出，所以，需要每一步都取余数
class Solution {
public:
    int hashCode(string key, int HASH_SIZE) {
        int l = key.size();
        long ret = 0;
        for(int i = 0; i < l; i++)
        {
        	ret = (ret * 33 + (int)key[i]) % HASH_SIZE;
        }

        return (int)ret;
    }
};



int main(void)
{
	Solution ss;
	cout << ss.hashCode("abcdefghijklmnopqrstuvwxyz", 2607) << endl;
	
	return 0;
}