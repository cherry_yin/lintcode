#include <iostream>
#include <vector>

using namespace std;

// DP, O(n^2)
class Solution1 {
public:
	int longestIncreasingSubsequence(vector<int> nums)
	{
		if (nums.empty())
			return 0;

		int l = nums.size();

		vector<int> hidden(l, 1);
		for(int i = 1; i < l; i++)
		{
			for(int j = i-1; j >= 0; j--)
			{
				if (nums[i] >= nums[j] && (hidden[i] <= hidden[j]))
					hidden[i] = hidden[j] + 1;
			}
		}
		
		return *max_element(hidden.begin(), hidden.end());
	}
};



int main(void)
{
	int a[] = {4,2,4,5,3,7};
	vector<int> ta(a, a + sizeof(a)/sizeof(int));

	Solution s;
	cout << s.longestIncreasingSubsequence(ta) << endl;
	return 0;
}