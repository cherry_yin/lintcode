#include <iostream>
using namespace std;

class ListNode{
public:
	int val;
	ListNode *next;
	ListNode(int val)
	{
		this->val = val;
		this->next = NULL;
	}
};


class Solution {
public:
	ListNode* reverseBetween(ListNode *head, int m, int n)
	{
		if(head == NULL || m >= n || m < 0)
			return head;

		//ListNode dummy = ListNode(0);
		ListNode *dummy = new ListNode(0);
		dummy->next = head;
		ListNode *node = dummy;

		for(int i = 1; i < m; i++)
		{
			if(node == NULL)
				return NULL;
			node = node->next;
		}
		
		ListNode *premNode = node;
		ListNode *mNode = node->next;
		ListNode *nNode = mNode;
		ListNode *postnNode = nNode->next;

		for(int i = m; i < n; i++)
		{
			if(postnNode == NULL)
				return NULL;

			ListNode *temp = postnNode->next;
			postnNode->next = nNode;
			nNode = postnNode;
			postnNode = temp;
		}
		premNode->next = nNode;
		mNode->next = postnNode;
		
		return dummy->next;
	}
};


int main(void)
{
	//1->2->3->4->5->null, m = 2, n = 4, 1->4->3->2->5->null
	ListNode n1(1);
	ListNode n2(2);
	ListNode n3(3);
	ListNode n4(4);
	ListNode n5(5);
	n1.next = &n2;
	n2.next = &n3;
	n3.next = &n4;
	n4.next = &n5;
	
	ListNode *h = &n1;
	Solution ss;
	h = ss.reverseBetween(h, 2, 4);
	
	while(h)
	{
		cout << h->val << endl;
		h = h->next;
	}

	return 0;
}