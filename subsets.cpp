#include <iostream>
#include <vector>
using namespace std;

class Solution0 {
public:
    /**
     * @param S: A set of numbers.
     * @return: A list of lists. All valid subsets.
     */
    vector<vector<int> > subsets(vector<int> &nums) {
        vector<vector<int> > ret;
        if(nums.empty())
        {
            return ret;
        }
        
        sort(nums.begin(), nums.end());
        vector<int> path;
        helper(nums, 0, path, ret);
        return ret;
    }

private:
    void helper(vector<int> &nums, int pos, vector<int> &path, vector<vector<int> > &ret)
    {
        ret.push_back(path);
        
        for(int i = pos; i < nums.size(); i++)
        {
            path.push_back(nums[i]);
            helper(nums, i + 1, path, ret);
            path.pop_back();
        }
    }
};


//non-recursion solution
//note1: the bracket cannot be ignored!! in """if((i & (1 << j)) != 0)"""
class Solution {
public:
    vector<vector<int> > subsets(vector<int> &nums)
    {
        vector<vector<int> > result;
        if(nums.empty() || nums.size() == 0)
        {
            return result;
        }

        for(int i = 0; i < (1 << nums.size()); i++)
        {
            vector<int> path;
            for(int j = 0; j < nums.size(); j++)
            {
                if((i & (1 << j)) != 0)
                {
                    path.push_back(nums[j]);
                }
            }
            result.push_back(path);
        }
        return result;
    }
};

int main(void)
{
    int a[] = {1,2,3};
    vector<int> va(a, a + sizeof(a)/sizeof(int));
    cout << "main:\t"<< va.size() << endl;
    Solution ss;
    ss.subsets(va);

    return 0;
}