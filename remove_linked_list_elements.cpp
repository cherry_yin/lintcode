#include <iostream>
using namespace std;

class ListNode{
public:
	int val;
	ListNode *next;
	ListNode(int val)
	{
		self->val = val;
		self->next = NULL;
	}
};


class Solution
{
public:
	ListNode *removeElements(ListNode *head, int val)
	{
		ListNode dummy(0);
		dummy.next = head;
		
		ListNode *prev = &dummy;
		ListNode *node = head;

		while(node != NULL)
		{
			if(node->val == val)
			{
				prev->next = node->next;
			}
			else
			{
				prev = prev->next;
			}
			
			node = node->next;
		}
		return dummy.next;
	}
};