#include <vector>
#include <iostream>

using namespace std;

class Solution {
public:
    /**
     * @param grid: a list of lists of integers.
     * @return: An integer, minimizes the sum of all numbers along its path
     */
    int minPathSum(vector<vector<int> > &grid) {
        if (grid.empty())
        {
        	return -1;
        }

        int m = grid.size();
        int n = grid[0].size(); 

        vector<vector<int> > ant(m, vector<int> (n, 0));
        

        ant[m-1][n-1] = grid[m-1][n-1];
        for(int i = n-2; i >= 0 ; i--)
        {
        	ant[m-1][i] = grid[m-1][i] + ant[m-1][i+1];
        }

        for(int j = m-2; j >= 0; j--)
        {
        	ant[j][n-1] = grid[j][n-1] + ant[j+1][n-1];
        }

        for(int i = m-2; i >= 0; i--)
        {
        	for(int j = n-2; j >= 0; j--)
        	{
        		ant[i][j] = min(ant[i+1][j], ant[i][j+1]) + grid[i][j];
        	}
        		
        }


        return ant[0][0];
    }
};

int main(void)
{
	int iarray[]={1, 2, 1, 1};

	vector<vector<int> > a;
	vector<int> a1(iarray, iarray + 2);
	vector<int> a2(iarray + 2, iarray + 4);
	a.push_back(a1);
	a.push_back(a2);
	Solution s;

	cout << s.minPathSum(a) << endl;
	
	return 0;
}
