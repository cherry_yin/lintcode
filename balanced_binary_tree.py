class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None

class Solution:
    def isBalanced(self, root):
        if self.maxDepth(root) == -1:
            return False
        else:
            return True

    def maxDepth(self, root):

        if root is None:
            return 0

        left = self.maxDepth(root.left)
        right = self.maxDepth(root.right)

        if (left == -1) or (right == -1) or abs(left-right) > 1:
            return -1
        else:
            return max(left, right) + 1

if __name__ == "__main__":
    a = TreeNode(2)
    a.left = TreeNode(1)
    a.right = TreeNode(3)
    a.left.right = TreeNode(4)
    a.left.right.right = TreeNode(5)
    print Solution().isBalanced(a)