## 0.

- 做一题会一类才是正道。贪心是典型的一道是一道，而非一道题普照一类题的典型。
- 多用 continue 是好 coding style 之一。根据是减少代码块缩进。
- Time complexity in coding interview
    + O(1)极少.
    + O(logN) binary search.
    + O(sqrt(N))分解质因数
    + O(n) 高频
    + O(nlogn)排序
    + O(n^2)数组，枚举，DP
    + O(n^3)数组，枚举，DP
    + O(2^n)组合相关的搜索
    + O(n!)排列有关的搜索

## 1.练习方法

- 代码 3 大块：异常处理、主体、返回。
- 代码风格
    1.变量名的意义
    2.缩进（语句块）
    3.空格（二元运算符两边）
    4.代码可读性和健壮性（即便if语句只有一行，也要加花括号）

## 2.二分搜索 binary search

- 递归vs非递归实现的权衡
- 二分法模板的四个要点. [通用模板](http://www.jiuzhang.com/solutions/binary-search/).
    + start + 1 < end
    + start + (end - start)/2
    + A[mid] == > <
    + A[start], A[end] ? target
- 两类二分法
    + 二分位置
    + 二分答案
- 理解binary search的三个层次
    + 头尾指针，取重点，判断往左/右走
    + 找到满足条件的第一个/最后一个位置
    + 保留生下来一定有解的一半
- [面经中的binary search](http://www.jiuzhang.com/qa/974/)


## 3.二叉树与分治 Binary Tree & Divide Conquer


## 4.动态规划 Dynamic Programming I

## 5.动态规划 Dynamic Programming II


## 6.链表 Linked List

链表是线性表的一种。线性表有两种存储方式，一种是顺序存储结构，另一种是链式存储结构。常用的数组就是一种典型的顺序存储结构。链表就是链式存储的线性表。

顺序表的特性是随机读取，也就是访问一个元素的时间复杂度是O(1)，链式表的特性是插入和删除的时间复杂度为O(1)。链式存储结构就是两个相邻的元素在内存中可能不是相邻的，每一个元素都有一个指针域，指针域一般是存储着到下一个元素的指针。这种存储方式的优点是插入和删除的时间复杂度为 O(1)，不会浪费太多内存，添加元素的时候才会申请内存，删除元素会释放内存。缺点是访问的时间复杂度最坏为 O(n)。

根据指针域的不同，链表分为单向链表、双向链表、循环链表等等。

实现方式：
```
#python
class ListNode:
    def __init__(self, val):
        self.val = val
        self.next = None
```

```
\\ cpp
struct ListNode {
    int val;
    ListNode *next;
    ListNode(int val,ListNode *next=NULL):val(val),next(next){}
};
```

链表的基本操作。

- 1.反转链表.
    
    单向链表.要注意：1).访问某个节点 curt.next 时，要检验 curt 是否为 null。2).要把反转后的最后一个节点（即反转前的第一个节点）指向 null。
    [reverse-linked-list/python-code](./reverse_linked_list.py)
    [reverse-linked-list/cpp-code](./reverse_linked_list.cpp)

- 2.双向链表翻转。

    和单向链表的区别在于：双向链表的反转核心在于`next`和`prev`域的交换，还需要注意的是当前节点和上一个节点的递推。

```python
class DListNode:
    def __init__(self, val):
        self.val = val
        self.prev = self.next = null

    def reverse(self, head):
        curt = None
        while head:
            curt = head
            head = curt.next
            curt.next = curt.prev
            curt.prev = head
        return curt
```

- 3.删除链表中的某个节点

删除链表中的某个节点一定需要知道这个点的前继节点，所以需要一直有指针指向前继节点。还有一种删除是伪删除，是指复制一个和要删除节点值一样的节点，然后删除，这样就不必知道其真正的前继节点了。由于链表表头可能被删除，所以需要引入dummy node.



## 链表指针的鲁棒性

综合上面讨论的两种基本操作，链表操作时的鲁棒性问题主要包含两个情况：
- 当访问链表中某个节点 curt.next 时，一定要先判断 curt 是否为 null。
- 全部操作结束后，判断是否有环；若有环，则置其中一端为 null。

## Dummy Node

Dummy node 是一个虚拟节点，也可以认为是标杆节点。Dummy node 就是在链表表头 head 前加一个节点指向 head，即 dummy -> head。Dummy node 的使用多针对单链表没有前向指针的问题，保证链表的 head 不会在删除操作中丢失。除此之外，还有一种用法比较少见，就是使用 dummy node 来进行head的删除操作，比如 Remove Duplicates From Sorted List II，一般的方法current = current.next 是无法删除 head 元素的，所以这个时候如果有一个dummy node在head的前面。

所以，当链表的 head 有可能变化（被修改或者被删除）时，使用 dummy node 可以很好的简化代码，最终返回 dummy.next 即新的链表。

## 快慢指针

快慢指针也是一个可以用于很多问题的技巧。所谓快慢指针中的快慢指的是指针向前移动的步长，每次移动的步长较大即为快，步长较小即为慢，常用的快慢指针一般是在单链表中让快指针每次向前移动2，慢指针则每次向前移动1。快慢两个指针都从链表头开始遍历，于是快指针到达链表末尾的时候慢指针刚好到达中间位置，于是可以得到中间元素的值。快慢指针在链表相关问题中主要有两个应用：
- 快速找出未知长度单链表的中间节点
    设置两个指针 `*fast`、`*slow` 都指向单链表的头节点，其中`*fast`的移动速度是`*slow`的2倍，当`*fast`指向末尾节点的时候，`slow`正好就在中间了。
- 判断单链表是否有环
    利用快慢指针的原理，同样设置两个指针 `*fast`、`*slow` 都指向单链表的头节点，其中 `*fast`的移动速度是`*slow`的2倍。如果 `*fast = NULL`，说明该单链表 以 `NULL`结尾，不是循环链表；如果 `*fast = *slow`，则快指针追上慢指针，说明该链表是循环链表。

### Python

```python
class NodeCircle:
    def __init__(self, val):
        self.val = val
        self.next = None

    def has_circle(self, head):
        slow = head
        fast = head
        while (slow and fast):
            fast = fast.next
            slow = slow.next
            if fast:
                fast = fast.next
            if fast == slow:
                break
        if fast and slow and (fast == slow):
            return True
        else:
            return False
```


## Basic Skills in Linked List

1.Insert a Node in Sorted List
2.Remove a Node from Linked List(增删查改)
3.Reverse a Linked List
4.Merge Two Linked Lists
5.Middle of a Linked List


## Sort List

## Reorder List

## Fast-slow Pointers
1.Middle of Linked list
2.Remove Nth Node from end of list
3.linked list cycle i, 知道怎么做，理解
4.linked list cycle ii, 知道怎么做，知道为什么，背程序
5.rotate list







- copy list with random pointers
    + 能写出来hash map的方法
    + no extra space的方法能够实现正确

- Merge k sorted lists
    + priority queue (heap)
    + divide conquer
    + merge lists two by two
    - K 路归并算法一定要掌握！！！
    - 三种实现方式，分别实现，熟练理解和掌握！！
    - 顺便做一下merge k sorted arrays



## 7.数组与数 Array & Numbers



## 8.数据结构 Data Structure


## 9.图与搜索 Graph & Search

- Graph (clone graph, topological sorting)
- Search






