"""
Definition of TreeNode:
"""
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

"""
Example of iterate a tree:
iterator = BSTIterator(root)
while iterator.hasNext():
    node = iterator.next()
    do something for node
"""

class BSTIterator:
    #@param root: The root of binary tree.
    def __init__(self, root):
        self.curt = root
        self.stack = []

    #@return: True if there has next node, or false
    def hasNext(self):
        if self.curt is not None or len(self.stack) > 0:
            return True
        else:
            return False

    #@return: return next node
    def next(self):

        while self.curt is not None:
            self.stack.append(self.curt)
            self.curt = self.curt.left

        self.curt = self.stack.pop()
        node = self.curt
        self.curt = self.curt.right

        return node


if __name__ == "__main__":
    a = TreeNode(2)
    a.left = TreeNode(1)
    a.right = TreeNode(3)

