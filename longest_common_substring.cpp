#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:    
    /**
     * @param A, B: Two string.
     * @return: the length of the longest common substring.
     */
    //int longestCommonSubstring(string &A, string &B) {
    int longestCommonSubstring(string A, string B) {
    	int la = A.length();
    	int lb = B.length();
    	int global_max = 0;

        vector<vector<int> > lcs(la + 1, vector<int>(lb + 1, 0));

        for(int i = 1; i <= la; i++)
        {
        	for(int j = 1; j <= lb; j++)
        	{
        		if ((A[i-1] == B[j-1]) && (A[i-2] == B[j-2]))
        		{
        			lcs[i][j] = lcs[i-1][j-1] + 1;
        			if (lcs[i][j] > global_max)
        				global_max = lcs[i][j];
        		}
        		else if(A[i-1] == B[j-1])
        			lcs[i][j] = 1;
        		else
        			lcs[i][j] = max(lcs[i][j-1], lcs[i-1][j]);
        	}
        }

        return global_max;
    }
};

int main(void)
{
	Solution s;
	cout << s.longestCommonSubstring("ABCD", "CBCE") << endl;
	return 0;
}