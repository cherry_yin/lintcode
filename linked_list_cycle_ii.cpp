#include <iostream>
using namespace std;


class ListNode
{
public:
	int val;
	ListNode *next;
	ListNode(int val)
	{
		this->val = val;
		this->next = NULL;
	}
};


class Solution {
public:
    /**
     * @param head: The first node of linked list.
     * @return: The node where the cycle begins.
     *           if there is no cycle, return null
     */
    ListNode *detectCycle(ListNode *head) {
        if (NULL == head || NULL == head->next) {
            return NULL;
        }

        ListNode *slow = head;
        ListNode *fast = head;
        while (fast != NULL && fast->next == NULL)
        {
            fast = fast->next->next;
            slow = slow->next;

            if (slow == fast) {
                fast = head;
                while (slow != fast) {
                    fast = fast->next;
                    slow = slow->next;
                }
                return slow;
            }
        }
        return NULL;
    }
};


int main(void)
{
    ListNode n1(1);
    ListNode n2(2);
    n1.next = &n2;

    Solution ss;
    ListNode *ret = ss.detectCycle(&n1);

    return 0;
}