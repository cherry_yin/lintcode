#include <iostream>
using namespace std;


class ListNode
{
public:
	int val;
	ListNode *next;
	ListNode(int val)
	{
		this->val = val;
		this->next = NULL;
	}
};

//merge sort
class Solution0
{
private:
	ListNode *findMiddle(ListNode *head)
	{
		if(head == NULL || head->next == NULL)
			return head;
		
		ListNode *fast = head;
		ListNode *slow = head;
		while(fast->next != NULL && fast->next->next != NULL)
		{
			fast = fast->next->next;
			slow = slow->next;
		}
		return slow;
	}

	ListNode *merge(ListNode *left, ListNode *right)
	{
		ListNode dummy(0);
		ListNode *node = &dummy;

		while(left != NULL && right != NULL)
		{
			if(left->val < right->val)
			{
				node->next = left;
				left = left->next;
			}
			else
			{
				node->next = right;
				right = right->next;
			}
			node = node->next;
		}

		if(left != NULL)
		{
			node->next = left;
		}
		else
		{
			node->next = right;
		}
		return dummy.next;
	}
	

public:
	ListNode *sortList(ListNode *head)
	{
		if(head == NULL || head->next == NULL)
		{
			return head;
		}

		ListNode *mid = findMiddle(head);

		ListNode *right = sortList(mid->next);
		mid->next = NULL;
		ListNode *left = sortList(head);

		return merge(left, right);
	}
};


// Quick sort
class Solution
{
public:
	ListNode *sortList(ListNode *head)
	{
		if(head == NULL || head->next == NULL)
			return head;

		ListNode *mid = findMiddle(head);

		ListNode leftDummy(0);
		ListNode rightDummy(0);
		ListNode middleDummy(0);

		ListNode *leftNode = &leftDummy;
		ListNode *rightNode = &rightDummy;
		ListNode *midNode = &middleDummy;

		
	}
};


int main(void)
{
	ListNode n1(1);
	ListNode n2(2);
	ListNode n3(3);
	n1.next = &n2;
	n2.next = &n3;
	Solution ss;
	ListNode *ret = ss.sortList(&n1);
	cout << ret->val << endl;

	return 0;
}