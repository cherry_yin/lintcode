#include <iostream>
#include <vector>

using namespace std;

class Solution{
public:
	int uniquePaths(int m, int n) {
		if (m < 1 || n < 1)
			return 0;
		
		vector<vector<int> > numPath(m, vector<int>(n, 1));

		for(int i=m-2; i >= 0; i--)
		{
			for(int j=n-2; j >= 0; j--)
			{
				numPath[i][j] = numPath[i+1][j] + numPath[i][j+1];
			}
		}

		return numPath[0][0];
	}

};

int main(void)
{
	Solution s;
	cout << s.uniquePaths(3,2) << endl;

	return 0;
}