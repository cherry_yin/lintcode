class Solution:
	"""
	@param A: a list of integers
	@param target: an integer to be searched
	@return: an interger
	"""
	def search(self, A, target):
		if A is None or len(A) == 0:
			return -1

		start = 0
		end = len(A) - 1
		while start + 1 < end:
			mid = (start + end)/2
			if A[mid] == target:
				return mid
			# pivot on right
			if A[mid] < A[end]:
				if A[mid] < target:

			# pivot on left

		if A[start] == target:
			return start
		else:
			return end

		return -1

if __name__ == "__main__":
	a = Solution
	print "Example 1:\t", a.search([4, 5, 1, 2, 3], 1)
	print "Example 2:\t", a.search([4, 5, 1, 2, 3], 0)