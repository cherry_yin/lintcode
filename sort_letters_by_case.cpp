#include <iostream>
#include <string>
using namespace std;


class Solution
{
public:
	void sortLetters(string &letters) 
	{
		int size = letters.size();
		int left = 0;
		int right = size - 1;

		while(left <= right)
		{
			while(letters[left] >= 'a' && letters[left] <= 'z')
			{
				left++;
			}

			while(letters[right] >= 'A' && letters[right] <= 'Z')
			{
				right--;
			}
			
			//cout << left <<"\t"<< right << endl;

			if(left <= right)
			{
				char temp = letters[right];
				letters[right] = letters[left];
				letters[left] = temp;
				left++;
				right--;
			}
		}

		//cout << letters << endl;
    }
};


int main(void)
{
	string ss = "DERLKAJKFKLAJFKAKLFJKLJFa";
	Solution s;
	s.sortLetters(ss);

	return 0;
}