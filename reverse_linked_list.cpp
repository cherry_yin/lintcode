#include <iostream>
using namespace std;

class ListNode{
public:
	int val;
	ListNode* next;
	ListNode(int val)
	{
		this->val = val;
		this->next = NULL;
	}
};

class Solution {
public:
	ListNode *reverse(ListNode *head)
	{
		if (head == NULL)
			return NULL;

		ListNode *prev = NULL;
		ListNode *temp;

		while(head != NULL)
		{
			temp = head->next;
			head->next = prev;
			prev = head;
			head = temp;
		}
		//notice here! The last head is NULL, 
		//so the returned node should be the previous one.
		head = prev;

		return head;
	}
};

int main(void)
{
	ListNode n1(1);
	ListNode n2(2);
	ListNode n3(3);
	n1.next = &n2;
	n2.next = &n3;

	Solution ss;
	ListNode *ret;
	ret = ss.reverse(&n1);
	while(ret)
	{
		cout << ret->val << endl;
		ret = ret->next;
	}

	return 0;
}