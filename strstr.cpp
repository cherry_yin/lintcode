#include <iostream>
using namespace std;


class Solution {
public:
    /**
     * Returns a index to the first occurrence of target in source,
     * or -1  if target is not part of source.
     * @param source string to be scanned.
     * @param target string containing the sequence of characters to match.
     */
    int strStr(const char *source, const char *target) {
        int s_len = strlen(source);
        int t_len = strlen(target);
        
        if(source == NULL || target == NULL || s_len < t_len)
        {
            return -1;
        }

        if(s_len == 0 && t_len == 0)
        {
        	return 1;
        }
        
        
        for(int i = 0; i < s_len - t_len; i++)
        {
            int j;
            for(j = 0; j < t_len; j++)
            {
                if(source[i+j] != target[j])
                {
                    break;
                }
            }
            
            if(j == t_len)
            {
                return i;
            }
        }
        return -1;
    }
};


int main(void)
{
	char s[] = "";
	char t[] = "";
	Solution ss;
	cout << ss.strStr(s, t) << endl;
	return 0;
}