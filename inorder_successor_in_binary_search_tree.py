# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None


class Solution1(object):
    """
    @param root <TreeNode>: The root of the BST.
    @param p <TreeNode>: You need find the successor node of p.
    @return <TreeNode>: Successor of p.
    """

    """
    1.get inorder traverse list
    2.find the next element in the inorderTraverse list as inorderSuccessor
    """

    def inorderTraverse(self, root):
        inorderList = []
        if root is None:
            return []

        # Divide
        left = self.inorderTraverse(root.left)
        right = self.inorderTraverse(root.right)

        # Conquer
        inorderList += left + [root] + right

        return inorderList


    def inorderSuccessor(self, root, p):
        if root is None:
            return None

        inorderList = self.inorderTraverse(root)
        
        if inorderList.index(p) < len(inorderList) -1:
            suc = inorderList[inorderList.index(p)+1]
            return suc
        else:
            return None


class Solution(object):
    def inorderSuccessor(self, root, p):
        if root is None or p is None:
            return None
        
        successor = None
        while root is not None and root.val != p.val:
            if root.val > p.val:
                successor = root
                root = root.left
            else:
                root = root.right
        
        if root is None:
            return None
        
        if root.right is None:
            return successor
        
        root = root.right
        while root.left is not None:
            root = root.left
        
        return root



if __name__ == "__main__":
    a = TreeNode(2)
    a.left = TreeNode(1)
    a.right = TreeNode(3)
    # print Solution().inorderTraverse(a)
    print Solution().inorderSuccessor(a, a).val