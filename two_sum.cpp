#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;


class Solution0 {
public:
    /*
     * @param numbers : An array of Integer
     * @param target : target = numbers[index1] + numbers[index2]
     * @return : [index1+1, index2+1] (index1 < index2)
     */
    vector<int> twoSum(vector<int> &nums, int target) 
    {
        vector<int> result;
        int length = nums.size();

        unordered_map<int, int> hash(length);
        for (int i = 0; i != length; ++i)
        {
            if (hash.find(target - nums[i]) != hash.end()) 
            {
                result.push_back(hash[target - nums[i]]);
                result.push_back(i + 1);
                return result;
            } 
            else 
            {
                hash[nums[i]] = i + 1;
            }
        }

        return result;
    }
};


class Solution {
public:
    /*
     * @param numbers : An array of Integer
     * @param target : target = numbers[index1] + numbers[index2]
     * @return : [index1+1, index2+1] (index1 < index2)
     */
    vector<int> twoSum(vector<int> &nums, int target) {
        vector<int> result;
        
        int length = nums.size();

        vector<pair<int, int> > num_index(length);
        
        for (int i = 0; i != length; ++i) {
            num_index[i].first = nums[i];
            num_index[i].second = i + 1;
        }

        sort(num_index.begin(), num_index.end());
        int start = 0, end = length - 1;
        while (start < end) {
            if (num_index[start].first + num_index[end].first > target) 
            {
                --end;
            } 
            else if(num_index[start].first + num_index[end].first == target) 
            {
                int min_index = min(num_index[start].second, num_index[end].second);
                int max_index = max(num_index[start].second, num_index[end].second);
                result.push_back(min_index);
                result.push_back(max_index);
                return result;
            } 
            else 
            {
                ++start;
            }
        }

        return result;
    }
};


int main(void)
{
	vector<int> a;
	a.push_back(2);
	a.push_back(7);
	a.push_back(11);
	a.push_back(15);

	Solution ss;
	vector<int> ret = ss.twoSum(a, 9);
	cout << ret[0] << "\t" << ret[1] << endl;

	unordered_map<int, int> hash(3);
	hash[1] = 2;
	hash[2] = 3;
	hash[3] = 4;
	cout << hash.end() << endl;

	return 0;
}