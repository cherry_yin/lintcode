#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

// solution 1: sort & merge
class Solution1 {
public:
    /**
     * @param nums1 an integer array
     * @param nums2 an integer array
     * @return an integer array
     */
    vector<int> intersection(vector<int>& nums1, vector<int>& nums2) {
    	sort(nums1.begin(), nums1.end());
    	sort(nums2.begin(), nums2.end());

    	vector<int> ret;

    	int i = 0;
    	int j = 0;
    	while(i < nums1.size() && j < nums2.size())
    	{
    		if(i > 0 && nums1[i] == nums1[i-1])
    		{
    			i++;
    			continue;
    		}

    		if(j > 0 && nums2[j] == nums2[j-1])
    		{
    			j++;
    			continue;
    		}

    		if(nums1[i] == nums2[j])
    		{
    			ret.push_back(nums1[i]);
    			i++;
    			j++;
    		}
    		else if(nums1[i] > nums2[j])
    		{
    			j++;
    		}
    		else
    		{
    			i++;
    		}
    	}

    	return ret;
    }
};



int main()
{
	Solution ss;

	int n1[] = {1, 2, 2, 1};
	int n2[] = {2, 2};

	vector<int> num1(n1, n1 + sizeof(n1) / sizeof(int));
	vector<int> num2(n2, n2 + sizeof(n2) / sizeof(int));

	vector<int> ret;
	ret = ss.intersection(num1, num2);
	for(int i = 0; i < ret.size(); i++)
	{
		cout << ret[i] << endl;
	}

	return 0;
}