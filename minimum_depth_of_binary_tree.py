class TreeNode:
	def __init__(self, val):
		self.val = val
		self.right = None
		self.left = None

class Solution:
    """
    @param root: The root of binary tree.
    @return: An integer
    """ 
    def minDepth(self, root):
        if root is None:
        	return 0

        left_min = self.minDepth(root.left)
        right_min = self.minDepth(root.right)

        minDep = min(left_min, right_min) + 1

        return minDep

if __name__ == "__main__":
	na = TreeNode(2)
	na.left = TreeNode(1)
	na.right = TreeNode(3)
	na.right.left = TreeNode(5)
	
	print Solution().minDepth(na)