"""
Definition of TreeNode:
"""
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None


class Solution:
    """
    @param root: The root of binary tree.
    @return: Postorder in ArrayList which contains node values.
    """
    def postorderTraversal(self, root):
        result = []
        if root is None:
            return []
        
        left_subTree = self.postorderTraversal(root.left)
        right_subTree = self.postorderTraversal(root.right)
        
        result = left_subTree + right_subTree + [root.val]
        
        return result


class Solution:
    """
    @param root: The root of binary tree.
    @return: Postorder in ArrayList which contains node values.
    """
    def postorderTraversal(self, root):
        result = []
        self.traverse(root, result)
        return result
    
    def traverse(self, root, result):
        if root is None:
            return
        
        self.traverse(root.left, result)
        self.traverse(root.right, result)
        result += [root.val] 