#include <iostream>
using namespace std;

class ListNode
{
public:
    int val:
    ListNode *next;
    ListNode(int val)
    {
        this-val = val;
        this->next = NULL;
    }
};

class Solution
{
public:
    ListNode *deleteDuplicates(ListNode *head)
    {
        if(head == NULL || head->next == NULL)
        {
            return head;
        }

        ListNode *node = head->next;
        ListNode *prev = head;
        while(node)
        {
            if(node->val == prev->val)
            {
                prev->next = node->next;
            }
            else
            {
                prev = prev->next;
            }
            node = node->next;
        }
        return head;
    }
};
