//  20160814
//  Space complexity: O(1), Time complexity: O(n);
//  Notice: 1)rotate from left to right; 2)limitation condition. i.offset > str_len; ii.str_len == 0.
class Solution {
public:
    /**
     * @param str: a string
     * @param offset: an integer
     * @return: nothing
     */
    void rotateString(string &str,int offset){
        int str_len = str.size();
        if(str_len == 0)
            return;
        
        offset = offset%str_len;
        if(offset == 0)
        {
            return;
        }
        
        reverse(str, 0, str.size()-1-offset);
        reverse(str, str.size()-offset, str.size()-1);
        reverse(str, 0, str.size()-1);
    }
    
private:
    void reverse(string &str, int start, int end)
    {
        while(start < end)
        {
            char temp = str[start];
            str[start] = str[end];
            str[end] = temp;
            start++;
            end--;
        }
    }
};
