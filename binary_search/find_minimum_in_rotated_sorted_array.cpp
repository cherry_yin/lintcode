#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
    /**
     * @param num: a rotated sorted array
     * @return: the minimum number in the array
     */
    int findMin(vector<int> &num) {
        if(num.empty() || num.size() == 0)
        	return 0;

        int start = 0;
        int end = num.size() - 1;
        int target = num[end];
        int mid;
        while(start + 1 < end)
        {
        	mid = start + (end - start)/2;
        	if(num[mid] <= target)
        	{
        		end = mid;
        	}
        	else
        	{
        		start = mid;
        	}
        }

        return min(num[start], num[end]);
    }
};

int main(void)
{
	int a[] = {4, 5, 6, 7, 0, 1, 2};
	vector<int> va(a, a + sizeof(a)/sizeof(int));

	Solution ss;
	cout << ss.findMin(va) << endl;

	return 0;
}
