//  20160809
//  Solution: find the index, and binary search in sorted array
//  注意find first/last的时候，while循环里面不能刚一找到就返回

/**
 * Definition of ArrayReader:
 * 
 * class ArrayReader {
 * public:
 *     int get(int index) {
 *          // return the number on given index, 
 *          // return -1 if index is less than zero.
 *     }
 * };
 */
class Solution {
public:
    /**
     * @param reader: An instance of ArrayReader.
     * @param target: An integer
     * @return: An integer which is the first index of target.
     */
    int searchBigSortedArray(ArrayReader *reader, int target) {
        int index = 1;
        while(reader->get(index) < target)
        {
            index = index * 2;
        }
        
        int start = index /2;
        int end = index;
        int mid;
        while(start + 1 < end)
        {
            mid = start + (end - start) / 2;
            if(reader->get(mid) < target)
            {
                start = mid;
            }
            else
            {
                end = mid;
            }
        }
        if(reader->get(start) == target)
        {
            return start;
        }
        
        if(reader->get(end) == target)
        {
            return end;
        }
        
        return -1;
    }
};