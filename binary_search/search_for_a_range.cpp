//20160814
class Solution {
    /** 
     *@param A : an integer sorted array
     *@param target :  an integer to be inserted
     *return : a list of length 2, [index1, index2]
     */
public:
    vector<int> searchRange(vector<int> &A, int target) {
        vector<int> ret(2, -1);
        if(A.empty() || A.size() == 0)
        {
            return ret;
        }
        int start = 0;
        int end = A.size()-1;
        int mid;
        while(start + 1 < end)
        {
            mid = start + (end - start)/2;
            if(A[mid] < target)
            {
                start = mid;
            }
            else
            {
                end = mid;
            }
        }
        if(A[start] == target)
        {
            ret[0] = start;   
        }
        else if(A[end] == target)
        {
            ret[0] = end;
        }
        else
        {
            return ret;
        }
        
        start = 0;
        end = A.size() - 1;
        while(start + 1 < end)
        {
            mid = start + (end - start)/2;
            if(A[mid] <= target)
            {
                start = mid;
            }
            else
            {
                end = mid;
            }
        }
        if(A[end] == target)
        {
            ret[1] = end;
        }
        else if(A[start] == target)
        {
            ret[1] = start;
        }
        
        return ret;
    }
};