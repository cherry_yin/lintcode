//  v0.20160814
//  binray search.
//  O(logN)
class Solution {
public:
    /**
     * @param A: An integers array.
     * @return: return any of peek positions.
     */
    int findPeak(vector<int> A) {
        int start = 1;
        int end = A.size() - 2;
        int mid;
        while(start + 1 < end)
        {
            mid = (start + end) /2;
            if(A[mid] > A[mid - 1] && A[mid] > A[mid + 1])
            {
                return mid;
            }
            else if(A[mid] < A[mid - 1])
            {
                end = mid;
            }
            else if(A[mid] < A[mid + 1])
            {
                start = mid;
            }
        }
        if(A[start] > A[end])
        {
            return start;
        }
        else
        {
            return end;
        }
    }
};
