//  20160814
//  rotate array三步翻转法
class Solution {
public:
    void recoverRotatedSortedArray(vector<int> &nums) {
        if(nums.empty() || nums.size() == 0)
            return;
        
        int end = nums.size() - 1;
        int rotate_pos = 0;
        for(int i = 0; i < end; i++)
        {
            if(nums[i] > nums[i+1])
            {
                rotate_pos = i;
                break;
            }
        }
        
        if(rotate_pos == 0)
            return;
        
        int start = 0;
        end = rotate_pos;
        while(start < end)
        {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
        
        start = rotate_pos + 1;
        end = nums.size() - 1;
        while(start < end)
        {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
        
        start = 0;
        end = nums.size() - 1;
        while(start < end)
        {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
    }
};