#include <iostream>
#include <vector>
using namespace std;

class Solution
{
public:
    int search(vector<int> A, int target)
    {
        if(A.empty() || A.size() == 0)
        {
            return -1;
        }

        int start = 0;
        int end = A.size() - 1;
        int mid;
        while(start + 1 < end)
        {
            mid = start + (end - start)/2;
            if(A[mid] == target)
            {
                return mid;
            }
            if(A[mid] < A[start])
            {
                if(A[mid] < target && A[start] >= target)
                {
                    start = mid;
                }
                else
                {
                    end = mid;
                }
            }
            else
            {
                if(A[mid] > target && A[start] <= target)
                {
                    end = mid;
                }
                else
                {
                    start = mid;
                }
            }
        }

        if(A[start] == target)
            return start;
        else if(A[end] == target)
            return end;
        return -1;
    }
};

int main(void)
{
    int a[] = {3,4,5, 1, 2};
    vector<int> va(a, a + sizeof(a)/sizeof(int));

    Solution ss;
    cout << ss.search(va, 3) << endl;

    return 0;
}
