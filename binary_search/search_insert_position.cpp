#include <iostream>
#include <vector>
using namespace std;

//20160809
//Solution: find the first position >= target
//Time Complexity: O(logN)
class Solution {
    /** 
     * param A : an integer sorted array
     * param target :  an integer to be inserted
     * return : an integer
     */
public:
    int searchInsert(vector<int> &A, int target) {
 		if(A.empty() || A.size() == 0)
 		{
 			return 0;
 		}

 		int start = 0;
 		int end = A.size() - 1;
 		int mid;
 		while(start + 1 < end)
 		{
 			mid = (start + end) / 2;
 			if(A[mid] == target)
 			{
 				return mid;
 			}
 			else if(A[mid] < target)
 			{
 				start = mid;
 			}
 			else
 			{
 				end = mid;
 			}
 		}
 		if(A[start] >= target)
 		{
 			return start;
 		}
 		else if(A[end] >= target)
 		{
 			return end;
 		}
 		else
 		{
 			return end + 1;
 		}
    }
};



//20160809
//Solution: find the last position < target, return +1， 要特判一下target小于所有数组里面的元素
//Time Complexity: O(logN)
class Solution {
    /** 
     * param A : an integer sorted array
     * param target :  an integer to be inserted
     * return : an integer
     */
public:
    int searchInsert(vector<int> &A, int target) {
    	if(A.empty() || A.size() == 0)
    		return 0;

    	int start = 0;
    	int end = A.size() - 1;
    	int mid;
    	while(start + 1 < end)
    	{
    		mid = start + (end - start)/2;
    		if(A[mid] <= target)
    		{
    			start = mid;
    		}
    		else
    		{
    			end = mid;
    		}
    	}
    	if(A[end] < target)
    	{
    		return end + 1;
    	}
    	else if(A[end] == target || A[start] < target)
    	{
    		return end;
    	}
    	else
    	{
    		return start;
    	}
    }
};
