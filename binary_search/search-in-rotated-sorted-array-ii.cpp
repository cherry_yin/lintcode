class Solution {
    /** 
     * param A : an integer ratated sorted array and duplicates are allowed
     * param target :  an integer to be search
     * return : a boolean 
     */
public:
    bool search(vector<int> &A, int target) {
        if(A.empty() || A.size() == 0)
        {
            return false;
        }
        
        for(int i = 0; i < A.size(); i++)
        {
            if(A[i] == target)
            {
                return true;
            }
        }
        return false;
    }
};

//只需要举出能够最坏情况的数据是 [1,1,1,1... 1] 里有一个0即可。
    // 在这种情况下是无法使用二分法的，复杂度是O(n)
    // 因此写个for循环最坏也是O(n)，那就写个for循环就好了