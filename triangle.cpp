#include <iostream>
#include <vector>
using namespace std;

//Traverse without hashmap
class Solution1{
public:
	/**
	* @param triangle: a list of lists of integers.
	* @return: An integer, minimum path sum.
	**/
	int minimumTotal(vector<vector<int> > &triangle)
	{
		if(triangle.empty())
		{
			return -1;
		}

		int result = INT_MAX;
		dfs(0, 0, 0, triangle, result);

		return result;
	}

private:
	void dfs(int x, int y, int sum, vector<vector<int> > &triangle, int &result)
	{
		const int n = triangle.size();
		if (x == n)
		{
			if (sum < result)
			{
				result = sum;
			}
			return;
		}
		dfs(x+1, y, (sum + triangle[x][y]), triangle, result);
		dfs(x+1, y+1, (sum + triangle[x][y]), triangle, result);
	}
};


//Divide and Conquer without hashmap
class Solution2{
public:
	int minimumTotal(vector<vector<int> > &triangle)
	{
		if(triangle.empty())
		{
			return -1;
		}

		int result = dfs(0, 0, triangle);

		return result;
	}

private:
	int dfs(int x, int y, vector<vector<int> > &triangle)
	{
		const int n = triangle.size();
		if (x == n)
		{
			return 0;
		}

		return min(dfs(x+1, y, triangle), dfs(x+1, y+1, triangle)) + triangle[x][y];
	}
};


//Divide and Conquer with hashmap
class Solution3 {
public:
    /**
     * @param triangle: a list of lists of integers.
     * @return: An integer, minimum path sum.
     */
    int minimumTotal(vector<vector<int> > &triangle) {
        if (triangle.empty()) {
            return -1;
        }

        vector<vector<int> > hashmap(triangle);
        for (int i = 0; i != hashmap.size(); ++i) {
            for (int j = 0; j != hashmap[i].size(); ++j) {
                hashmap[i][j] = INT_MIN;
            }
        }
        int result = dfs(0, 0, triangle, hashmap);

        return result;
    }

private:
    int dfs(int x, int y, vector<vector<int> > &triangle, vector<vector<int> > &hashmap) {
        const int n = triangle.size();
        if (x == n) {
            return 0;
        }

        if (hashmap[x][y] != INT_MIN) {
            return hashmap[x][y];
        }
        int x1y = dfs(x + 1, y, triangle, hashmap);
        int x1y1 = dfs(x + 1, y + 1, triangle, hashmap);
        hashmap[x][y] =  min(x1y, x1y1) + triangle[x][y];

        return hashmap[x][y];
    }
};


// DP - From Bottom to Top
class Solution4 {
public:
    /**
     * @param triangle: a list of lists of integers.
     * @return: An integer, minimum path sum.
     */
    int minimumTotal(vector<vector<int> > &triangle) {
        if (triangle.empty()) {
            return -1;
        }

        vector<vector<int> > hashmap(triangle);

        const int N = triangle.size();
        for (int i = 0; i != N; ++i) {
            hashmap[N-1][i] = triangle[N-1][i];
        }

        for (int i = N - 2; i >= 0; --i) {
            for (int j = 0; j < i + 1; ++j) {
                hashmap[i][j] = min(hashmap[i + 1][j], hashmap[i + 1][j + 1]) + triangle[i][j];
            }
        }

        return hashmap[0][0];
    }
};


// DP - From Top to Bottom
class Solution {
public:
    /**
     * @param triangle: a list of lists of integers.
     * @return: An integer, minimum path sum.
     */
    int minimumTotal(vector<vector<int> > &triangle) {
        if (triangle.empty()) {
            return -1;
        }

        vector<vector<int> > hashmap(triangle);

        // get the total row number of triangle
        const int N = triangle.size();
        //hashmap[0][0] = triangle[0][0];
        for (int i = 1; i != N; ++i) {
            for (int j = 0; j <= i; ++j) {
                if (j == 0) {
                    hashmap[i][j] = hashmap[i - 1][j];
                }
                if (j == i) {
                    hashmap[i][j] = hashmap[i - 1][j - 1];
                }
                if ((j > 0) && (j < i)) {
                    hashmap[i][j] = min(hashmap[i - 1][j], hashmap[i - 1][j - 1]);
                }
                hashmap[i][j] += triangle[i][j];
            }
        }

        int result = INT_MAX;
        for (int i = 0; i != N; ++i) {
            result = min(result, hashmap[N - 1][i]);
        }
        return result;
    }
};


int main(void)
{	
	vector<vector<int> > t;
	
	vector<int> t0;
	t0.push_back(2);
	t.push_back(t0);
	
	vector<int> t1;
	t1.push_back(3);
	t1.push_back(4);
	t.push_back(t1);

	vector<int> t2;
	t2.push_back(6);
	t2.push_back(5);
	t2.push_back(7);
	t.push_back(t2);

	vector<int> t3;
	t3.push_back(4);
	t3.push_back(1);
	t3.push_back(8);
	t3.push_back(3);
	t.push_back(t3);

	Solution s;
	cout << s.minimumTotal(t) << endl;

	return 0;
}