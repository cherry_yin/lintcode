class Solution {
public:
    /**
     * @param A and B: sorted integer array A and B.
     * @return: A new sorted integer array
     */
    vector<int> mergeSortedArray(vector<int> &A, vector<int> &B) {
        if(A.size() == 0)
            return B;
        if(B.size() == 0)
            return A;

        int sizeA = A.size();
        int sizeB = B.size();

        vector<int> ret(sizeA + sizeB, 0);
        int i = 0, j = 0;
        while(i<sizeA && j <sizeB)
        {
            if(A[i] <= B[j])
            {
                ret[i+j] = A[i];
                i++;
            }
            else
            {
                ret[i+j] = B[j];
                j++;
            }
        }

        while(i < sizeA)
        {
            ret[i+j] = A[i];
            i++;
        }

        while(j < sizeB)
        {
            ret[i+j] = B[j];
            j++;
        }

        return ret;
    }
};
