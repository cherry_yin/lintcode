#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution {
public:
	 /**
     * Determine whether s3 is formed by interleaving of s1 and s2.
     * @param s1, s2, s3: As description.
     * @return: true of false.
     */
	bool isInterleave(string s1, string s2, string s3)
	{

		int len_s1 = s1.size();
		int len_s2 = s2.size();
		int len_s3 = s3.size();

		if(len_s3 != (len_s1 + len_s2))
			return false;

		vector<vector<bool> > ret(1 + len_s1, vector<bool>(1 + len_s2, true));
		for(int i = 1; i <= len_s1; ++i)
		{
			ret[i][0] = (ret[i-1][0] && (s1[i-1] == s3[i-1]));
		}

		for(int i = 1; i <= len_s2; i++)
		{
			ret[0][i] = (ret[0][i-1] && (s2[i-1] == s3[i-1]));
		}

		for(int i = 1; i <= len_s1; i++)
		{
			for(int j = 1; j <= len_s2; j++)
			{
				ret[i][j] = ((ret[i-1][j] && (s1[i-1] == s3[i+j-1])) || (ret[i][j-1] && (s2[j-1] == s3[i+j-1])));
			}
		}

		return ret[len_s1][len_s2];
	}
};

int main(void)
{
	string str1 = "aabcc";
	string str2 = "dbbca";
	string str3 = "aadbbcbcac";
	string str4 = "aadbbbaccc";

	Solution ss;
	cout << ss.isInterleave(str1, str2, str3) << endl;
	cout << ss.isInterleave(str1, str2, str4) << endl;

	return 0;
}