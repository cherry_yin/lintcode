#include <iostream>
#include <vector>
#include <stack>
#include <algorithm>

using namespace std;

// O(n^2), TLE
// traverse width begin/end with [i, j]. find the min height, compute area.
class Solution0 {
public:
    /**
     * @param height: A list of integer
     * @return: The area of largest rectangle in the histogram
     */
    int largestRectangleArea(vector<int> &height)
    {
    	int max_area = 0;
    	if(height.size() == 0)
    	{
    		return 0;
    	}

    	int width;
    	int area;
    	for(int i = 0; i < height.size(); i++)
    	{
    		int min_height = height[i];
    		for(int j = i; j < height.size(); j++)
    		{
    			width = j - i + 1;
    			if(height[j] < min_height)
    			{
    				min_height = height[j];
    			}

    			area = width * min_height;
    			if(area > max_area)
    			{
    				max_area = area;
    			}
    		}
    	}
    	return max_area;
    }
};


class Solution {
public:
    int largestRectangleArea(vector<int> &height) 
    {
    	if(height.size() == 0)
    	{
    		return 0;
    	}

    	int max_area = 0;
    	height.push_back(0);
    	stack<int> s;

    	for(int i = 0; i < height.size(); i++)
    	{
            while(!s.empty() && height[s.top()] > height[i])
            {
                int idx = s.top(); 
                s.pop();
                int width = s.empty() ? i : (i-s.top()-1);
                max_area = max(max_area, height[idx] * width);
            }
            
            s.push(i);
    	}

        return max_area;
    }
};


int main(void)
{
	int h[] = {0};
	vector<int> vec_h(h, h + sizeof(h)/sizeof(int));

	Solution ss;
	cout << ss.largestRectangleArea(vec_h) << endl;

	return 0;
}
