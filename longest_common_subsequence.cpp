#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution {
public:
	int longestCommonSubsequence(string A, string B) {
		if (A.empty() || B.empty())
			return 0;

  		int lenA = A.length();
  		int lenB = B.length();

  		vector<vector<int> > lcs(lenA+1, vector<int>(lenB+1, 0));

  		for(int i = 1; i <= lenA; i++)
  		{
  			for(int j = 1; j <= lenB; j++)
  			{
  				if (A[i-1] == B[j-1])
  					lcs[i][j] = lcs[i-1][j-1] + 1;
  				else
  					lcs[i][j] = max(lcs[i-1][j], lcs[i][j-1]);
  			}
  		}

  		return lcs[lenA][lenB];
    }
};

int main(void)
{
	Solution s;
	cout << s.longestCommonSubsequence("ABCD", "EACB") << endl;
	return 0;

}