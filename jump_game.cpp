#include <iostream>
#include <vector>

using namespace std;

// DP
// O(n^2), lintcode TLE.
class Solution0 {
public:
    /**
     * @param A: A list of integers
     * @return: The boolean answer
     */
    bool canJump(vector<int> A) {
        if (A.empty()) {
        	return true;
        }

        int m = A.size();
        vector<bool> jumpto(m, false);
        jumpto[0] = true;

        for(int i=1; i < m; i++)
        {
        	for(int j=i-1; j >= 0; i++)
        	{
        		if(jumpto[j] && (j + A[j] >= i))
        		{
        			jumpto[i] = true;
        			break;
        		}
        	}
        }
        return jumpto[m - 1];
    }
};


// Greedy, fom bottom to top
class Solution {
public:
	bool canJump(vector<int> A) {
		if (A.empty())
		{
			return true;
		}

		int index_true = A.size() - 1;
		for(int i = A.size() - 2; i > -1; i--)
		{
			if( i + A[i] >= index_true)
			{
				index_true = i;
			}
		}

		if (index_true == 0)
			return true;
		else
			return false;
	}
};



int main(void)
{
	int a[] = {2, 3, 1, 1, 4};
	vector<int> ta(a, a+5);

	Solution s;
	s.canJump(ta);
	return 0;
}
