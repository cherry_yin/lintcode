class TreeNode:
	def __init__(self, val):
		self.val = val
		self.left = None
		self.right = None

# Divide Conquer
class Solution1:
	def inorderTraversal(self, root):
		result = []
		if root is None:
			return []
		
		left_sub_tree = self.inorderTraversal(root.left)
		right_sub_tree = self.inorderTraversal(root.right)

		result += left_sub_tree + [root.val] + right_sub_tree

		return result

class Solution:
	def inorderTraversal(self, root):
		result = []
		self.traverse(root, result)
		return result

	def traverse(self, root, result):
		if root is None:
			return

		self.traverse(root.left, result)
		result += [root.val]
		self.traverse(root.right, result)


if __name__ == "__main__":
	na = TreeNode(2)
	na.left = TreeNode(1)
	na.right = TreeNode(3)

	print Solution().inorderTraversal(na)
