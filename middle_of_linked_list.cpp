#include <iostream>
using namespace std;

class ListNode
{
public:
	int val;
	ListNode *next;
	ListNode(int val)
	{
		this->val = val;
		this->next = NULL;
	}	
};

class Solution
{
public:
	ListNode *middleNode(ListNode *head)
	{
		if((head == NULL) || (head->next == NULL))
			return head;

		ListNode *fastNode = head;
		ListNode *slowNode = head;
		while(fastNode->next)
		{
			if(fastNode->next->next == NULL)
			{
				return slowNode;
			}
			fastNode = fastNode->next->next;
			slowNode = slowNode->next;
		}

		return slowNode;
	}
};

int main(void)
{
	ListNode n1(1);
	ListNode n2(2);
	ListNode n3(3);
	ListNode n4(4);
	n1.next = &n2;
	n2.next = &n3;
	//n3.next = &n4;

	Solution ss;
	ListNode *ret = ss.middleNode(&n1);
	cout << ret->val << endl;
	return 0;
}