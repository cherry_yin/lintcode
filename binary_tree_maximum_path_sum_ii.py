
class Solution():
	def maxPathSum2(self, root):
		if root is None:
			return -99999

		left = self.maxPathSum2(root.left)
		right = self.maxPathSum2(root.right)

		return root.val + max(0, max(left, right))

