#include <iostream>
#include <string>
#include <cmath> 
using namespace std;


bool is_vanish(int cr, int cb, int cy, int x, int y, int z)
{
    if(abs(cr - cb) == x && abs(cr - cy) == y && abs(cb - cy) == z)
        return true;

    if(abs(cr - cb) == x && abs(cr - cy) == z && abs(cb - cy) == y)
        return true;

    if(abs(cr - cb) == y && abs(cr - cy) == x && abs(cb - cy) == z)
        return true;

    if(abs(cr - cb) == y && abs(cr - cy) == z && abs(cb - cy) == x)
        return true;

    if(abs(cr - cb) == z && abs(cr - cy) == x && abs(cb - cy) == y)
        return true;

    if(abs(cr - cb) == z && abs(cr - cy) == y && abs(cb - cy) == x)
        return true;

    return false;
}

int max_balls(string balls, int x, int y, int z)
{
    if(balls.empty() || balls.size() == 0)
        return 0;
    int max_sum = 0;
    int sum = 0;
    int cr, cy, cb;
    for(int i = 0; i < balls.size(); i++)
    {
        if(balls[i] == 'R')
            cr++;
        if(balls[i] == 'B')
            cb++;
        if(balls[i] == 'Y')
            cy++;

        sum++;
        max_sum = max(max_sum, sum);
        if(is_vanish(cr, cb, cy, x, y, z))
        {
            sum = 0;
            cr = 0;
            cb = 0;
            cy = 0;
        }
    }
    
    return max_sum;
}

int main(void)
{
    int x, y, z;
    cin >> x >> y >> z;
    string balls;
    cin >> balls;
    cout << max_balls(balls, x, y, z) << endl;
    return 0;
}
