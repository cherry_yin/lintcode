#include <iostream>
#include <vector>
using namespace std;

int dfs(int m[105][105], int row, int col)
{
	int ret = 0;
	vector<vector<bool> > visited(row, vector<bool>(col, false));
	for(int i = 0; i < row; i++)
	{
		for(int j = 0; j < col; j++)
		{
			if(m[i][j] == 1 && visited[i][j] == false)
			{
				
				dfs(m, row, col)
			}
		}
	}

	
	return 0;
}

int main(void)
{
	int T;
	cin >> T;
	int R, C;
	int a[105][105];
	int N;
	char command;
	int x, y, z;

	while(T)
	{
		cin >> R >> C;
		for(int i = 0; i < R; i++)
		{
			for(int j = 0; j < C; j++)
				cin >> a[i][j];
		}

		cin >> N;
		vector<int> connected_num;
		while(N--)
		{
			cin >> command;
			if(command == 'M')
			{
				cin >> x >> y >> z;
				a[x][y] = z;
			}
			else if(command == 'Q')
			{
				connected_num.push_back(dfs(a, R, C));
			}
		}

		cout << "Case #" << T << ":" << endl;
		for(int i = 0; i < connected_num.size(); i++)
		{
			cout << connected_num[i] << endl;
		}

		T--;
	}
	return 0;
}
