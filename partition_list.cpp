#include <iostream>
using namespace std;

class ListNode 
{
public:
	int val;
	ListNode *next;
	ListNode(int val)
	{
		this->val = val;
		this->next = NULL;
	}
};

class Solution {
public:
	ListNode *partition(ListNode *head, int x)
	{
		if(head == NULL)
			return NULL;

		ListNode dummy1 = ListNode(0);
		ListNode dummy2 = ListNode(0);
		ListNode *node1 = &dummy1;
		ListNode *node2 = &dummy2;

		while(head)
		{
			if(head->val < x)
			{
				node1->next = head;
				node1 = node1->next;
			}
			else
			{
				node2->next = head;
				node2 = node2->next;
			}
			head = head->next;
		}

		//connect left and right list
		node1->next = dummy2.next;
		//Notice!!!!!不然会死循环。
		node2->next = NULL;

		return dummy1.next;
	}
};

int main(void)
{
	ListNode n1(1);
	ListNode n2(4);
	ListNode n3(3);
	ListNode n4(2);
	ListNode n5(5);
	ListNode n6(2);
	n1.next = &n2;
	n2.next = &n3;
	n3.next = &n4;
	n4.next = &n5;
	n5.next = &n6;

	Solution ss;
	ListNode *ret = ss.partition(&n1, 3);
	while(ret)
	{
		cout << ret->val << endl;
		ret = ret->next;
	}
	return 0;
}