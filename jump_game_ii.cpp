#include <iostream>
#include <vector>
using namespace std;

//greedy
class Solution {
public:
    /**
     * @param A: A list of lists of integers
     * @return: An integer
     */
    int jump(vector<int> A) 
    {
        if (A.empty())
            return 0;

        int m = A.size();
        int jumps = 0;
        int end = m - 1;
        int min_index = m - 1;

        while (end > 0) 
        {
            for (int i = end - 1; i >= 0; --i) 
            {
                if (i + A[i] >= end) 
                {
                    min_index = i;
                }
            }

            if (min_index < end) 
            {
                ++jumps;
                end = min_index;
            } 
            else 
                return -1;
        }

        return jumps;
    }
};

int main(void)
{
	int a[] = {2, 3, 1, 1, 4};
	vector<int> va(a, a+5);

	Solution s;
	cout << s.jump(va) << endl;
	return 0;
}