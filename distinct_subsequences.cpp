#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution {
public:
	int numDistinct(string &S, string &T)
	{
		if (T.empty())
			return 1;

		if(S.size() < T.size())
			return 0;

		int len_s = S.size();
		int len_t = T.size();
		vector<vector<int> > f(len_s + 1, vector<int>(len_t + 1, 0));
		
		for(int i = 1; i <= len_s; i++)
		{
			f[i-1][0] = 1;
			for(int j = 1; j <= len_t; j++)
			{
				if(T[j-1] == S[i-1])
					f[i][j] = f[i-1][j-1] + f[i-1][j];
				else
					f[i][j] = f[i-1][j];
			}
		}
		return f[len_s][len_t];
	}
};

int main(void)
{
	string s = "rabbbit";
	string t = "rabbit";
	Solution ss;
	cout << ss.numDistinct(s, t) << endl;

	return 0;
}