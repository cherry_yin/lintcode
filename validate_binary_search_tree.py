#!/usr/bin/env python
# encoding: utf-8

# Definition for a binary tree node
class TreeNode:
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

# Solution 1: Divide Conquer
class Solution:
    """
    @param root: The root of binary tree.
    @return: True if the binary tree is BST, or fasle
    """
    def isValidBST(self, root):
        return self.isValidBSTRec(root, float("-inf"), float("inf"))

    def isValidBSTRec(self, root, low, high):
        if root is None:
            return True

        return low < root.val and root.val < high \
                and self.isValidBSTRec(root.left, low, root.val) \
                and self.isValidBSTRec(root.right, root.val, high)


# Solution 2: Traverse
class Solution:
    def isValidBST(self, root):
        self.lastVal = float('-inf')
        self.isBST = True
        self.validate(root)
        return self.isBST

    def validate(self, root):
        if root is None:
            return
        self.validate(root.left)
        if self.lastVal >= root.val:
            self.isBST = False
            return
        self.lastVal = root.val
        self.validate(root.right)



if __name__ == "__main__":
    root = TreeNode(2)
    root.left = TreeNode(1)
    root.right = TreeNode(3)

    print Solution().isValidBST(root)
