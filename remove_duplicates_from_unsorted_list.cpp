#include <iostream>
using namespace std;

class ListNode
{
public:
    int val;
    ListNode *val;
    ListNode(int val)
    {
        this->val = val;
        this->next = NULL;
    }
};

class Solution
{
public:
    ListNode *removeDuplicates(ListNode *head)
    {
        if(head == NULL || head->next == NULL)
        {
            return head;
        }

        ListNode *node = head;
        ListNode *prev = head;

        node = node->next;
        while(node)
        {
            ListNode *fromStart = head;
            while(fromStart != node)
            {
                if(fromStart->val == node->val)
                {
                    prev->next = node->next;
                    break;
                }
                fromStart = fromStart->next;
            }

            if(fromStart == node)
            {
                prev = prev->next;
            }
            node = node->next;
        }
        return head;
    }
};
