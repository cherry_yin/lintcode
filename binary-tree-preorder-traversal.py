class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left = None
        self.right = None


# version 0: Non-Recursion(Recommend)
class Solution0:
    """
    @param root: The root of binary tree.
    @return: Preorder in ArrayList which contains node values.
    """
    def preorderTraversal(self, root):
        if root is None:
        	return []

        stack = [root]
        preorder = []
        while stack:
        	node = stack.pop()
        	preorder.append(node.val)
        	if node.right:
        		stack.append(node.right)
        	if node.left:
        		stack.append(node.left)
        
        return preorder


#Version 1: Traverse
class Solution:
    """
    @param root: The root of binary tree.
    @return: Preorder in ArrayList which contains node values.
    """
    def preorderTraversal(self, root):
        result = []
        self.traverse(root, result)
        return result

    def traverse(self, root, result):
        if root is None:
            return
        result.append(root.val)
        self.traverse(root.left, result)
        self.traverse(root.right, result)


# solution 2: Devide & Conquer
class Solution2():
    # @param root, a tree node
    # @return a list of integers
    def preorderTraversal(self, root):
        result = []
        
        if root is None:
            return result

        # divide
        left = self.preorderTraversal(root.left)
        right = self.preorderTraversal(root.right)

        # conquer
        result.append(root.val)
        result += left
        result += right
        return result


if __name__ == "__main__":
    
    root = TreeNode(1)
    root.right = TreeNode(2)
    root.right.left = TreeNode(3)
    result = Solution().preorderTraversal(root)
    print result