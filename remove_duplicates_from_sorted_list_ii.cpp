#include <iostream>
using namespace std;


class ListNode{
public:
	int val;
	ListNode *next;
	ListNode(int val)
	{
		this->val = val;
		this->next = NULL;
	}
};


class Solution {
public:
	ListNode* deleteDuplicates(ListNode* head)
	{
		if(head == NULL)
			return NULL;

		ListNode dummy(0);
		dummy.next = head;

		ListNode *node = &dummy;

		while(node->next != NULL && node->next->next != NULL)
		{
			if (node->next->val == node->next->next->val)
			{
				int dupNum = node->next->val;
				while(node->next != NULL && node->next->val == dupNum)
				{
					node->next = node->next->next;
				}
			}
			else
			{
				node = node->next;
			}
		}

		return dummy.next;
	}
};


int main(void)
{
	ListNode n1(1);
	ListNode n2(2);
	ListNode n3(3);
	ListNode n4(3);
	ListNode n5(4);
	ListNode n6(4);

	n1.next = &n2;
	n2.next = &n3;
	n3.next = &n4;
	n4.next = &n5;
	n5.next = &n6;

	Solution ss;
	ListNode *ret;
	ret = ss.deleteDuplicates(&n1);

	while(ret)
	{
		cout << ret->val << endl;
		ret = ret->next;
	}

	return 0;
}