// 20160804
// recursive version
class Solution {
public:
    /**
     * @param S: A set of numbers.
     * @return: A list of lists. All valid subsets.
     */
    vector<vector<int> > subsetsWithDup(const vector<int> &S) {
        vector<vector<int> > result;
        if(S.empty() || S.size() == 0)
        {
            return result;
        }
        vector<int> s_nums = S;
        sort(s_nums.begin(), s_nums.end());
        vector<int> path;
        helper(s_nums, 0, path, result);
        return result;
    }
    
    void helper(vector<int> s_nums, int pos, vector<int> &path, vector<vector<int> > &result)
    {
        result.push_back(path);
        
        for(int i = pos; i < s_nums.size(); i++)
        {
            if(i != pos && s_nums[i] == s_nums[i-1])
            {
                continue;
            }
            path.push_back(s_nums[i]);
            helper(s_nums, i + 1, path, result);
            path.pop_back();
        }
    }
};
