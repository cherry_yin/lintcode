#include <iostream>
using namespace std;


class ListNode {
public:
	int val;
	ListNode *next;
	ListNode(int val)
	{
		this->val = val;
		this->next = NULL;
	}
};


class Solution0 {
public:
	ListNode *removeNthFromEnd(ListNode *head, int n) {
		if(head == NULL || n < 0)
			return NULL;

		ListNode *fastNode = head;
		ListNode *preRemoveNode = head;

		for(int i = 0; i < n; i++)
		{
			if (fastNode == NULL)
				return NULL;
			fastNode = fastNode->next;
		}

		//Notice!! The condition of head node is deleted!!!!!!!
		if (fastNode == NULL)
			return head->next;

		while(fastNode->next)
		{
			fastNode = fastNode->next;
			preRemoveNode = preRemoveNode->next;
		}

		preRemoveNode->next = preRemoveNode->next->next;
		return head;
    }
};


class Solution {
public:
	ListNode *removeNthFromEnd(ListNode *head, int n)
	{
		if(head == NULL || n<0)
			return NULL;

		ListNode dummy(0);
		dummy.next = head;

		ListNode *fastNode = &dummy;
		ListNode *preRemoveNode = &dummy;

		for(int i = 0; i < n; i++)
		{
			fastNode = fastNode->next;
		}

		while(fastNode->next)
		{
			fastNode = fastNode->next;
			preRemoveNode = preRemoveNode->next;
		}

		preRemoveNode->next = preRemoveNode->next->next;

		return dummy.next;
	}
};

int main(void)
{
	ListNode n1(1);
	ListNode n2(2);
	ListNode n3(3);
	ListNode n4(4);
	ListNode n5(5);
	n1.next = &n2;
	n2.next = &n3;
	n3.next = &n4;
	n4.next = &n5;
	n5.next = NULL;

	ListNode *node = &n1;

	Solution ss;
	node = ss.removeNthFromEnd(node, 5);
	while(node)
	{
		cout << node->val << endl;
		node = node->next;
	}
	//delete n1;
	//delete n2;

	return 0;
}