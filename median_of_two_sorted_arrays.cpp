// 两个数组先归并成 (m+n)的数组，然后再算中位数
// 归并时间O(m+n)，中位数O(1)。The overall time complexity is O(m+n)
class Solution0 {
public:
    /**
     * @param A: An integer array.
     * @param B: An integer array.
     * @return: a double whose format is *.5 or *.0
     */
    double findMedianSortedArrays(vector<int> A, vector<int> B) {
        int sizeA = A.size();
        int sizeB = B.size();

        vector<int> m(sizeA+sizeB, 0);
        int i = 0, j = 0;
        while(i < sizeA && j < sizeB)
        {
            if(A[i] <= B[j])
            {
                m[i+j] = A[i];
                i++;
            }
            else
            {
                m[i+j] = B[j];
                j++;
            }
        }

        while(i < sizeA)
        {
            m[i+j] = A[i];
            i++;
        }

        while(j < sizeB)
        {
            m[i+j] = B[j];
            j++;
        }

        if((sizeA + sizeB)%2 == 0)
        {
            return (m[(sizeA + sizeB)/2] + m[(sizeA + sizeB)/2-1])/2.0;
        }
        else
            return 1.0 * m[(sizeA + sizeB)/2];
    }
};



