#!/usr/bin/env python
# encoding: utf-8

class TreeNode:
    def __init__(self, val):
        self.left = None
        self.right = None
        self.val = val

class Solution:
    def maxPathSum(self, root):
    	return self.dfs(root)[0]

    def dfs(self, root):
    	if root is None:
    		maxPath = -9999
    		singleMaxPath = 0
    		return maxPath, singleMaxPath

    	# Divide
    	left = self.dfs(root.left)
    	right = self.dfs(root.right)

    	# Conquer
    	maxPath = max(left[0], right[0], root.val + left[1] + right[1])
    	singleMaxPath = max(left[1] + root.val, right[1] + root.val, 0)
    	return maxPath, singleMaxPath

if __name__ == "__main__":
	a = TreeNode(2)
	a.left = TreeNode(1)
	a.right = TreeNode(3)

	print Solution().maxPathSum(a)