#include <iostream>
#include <string>
#include <vector>
using namespace std;

class Solution {
public:
	int minCut(string s)
	{
		if(s.empty())
			return 0;

		vector<int> cut(s.size() + 1, 0);

		vector<vector<bool> > pMat = getPalindromeMat(s);

		for(int i = 1; i <= s.size(); i++)
		{
			cut[i] = i;

			for(int j = 0; j < i; j++)
			{
				if(pMat[j][i-1])
				{
					cut[i] = min(cut[i], cut[j] + 1);
				}
			}
		}

		return cut[s.size()] -1;
	}

private:
	vector<vector<bool> > getPalindromeMat(string s)
	{
		int s_len = s.size();
		vector<vector<bool> > is_palindrome_mat(s_len, vector<bool>(s_len, true));
		
		for(int j = 1; j < s_len; j++)
		{
			for(int i = 0; i < j; i++)
			{
				is_palindrome_mat[i][j] = ((s[i] == s[j]) && is_palindrome_mat[i + 1][j - 1]);
			}
		}

		return is_palindrome_mat;
	}

};

int main(void)
{
	Solution s;
	// s.minCut("aa");
	cout << s.minCut("aab") << endl;
	return 0;
}