#include <iostream>
#include <unordered_map>
#include <map>
#include <vector>
using namespace std;

class Solution {
public:
    /**
     * @param nums: A list of integers
     * @return: A list of integers includes the index of the first number 
     *          and the index of the last number
     */
    vector<int> subarraySum(vector<int> nums)
    {
        map<int, int> hash;
        
        int sum = 0;
        hash[0] = -1;
        for (int i = 0; i < nums.size(); i++)
        {
            sum += nums[i];
            if (hash.find(sum) != hash.end())
            {
                vector<int> result;
                result.push_back(hash[sum] + 1);
                result.push_back(i);
                return result;
            }
            hash[sum] = i;   
        }
        
        vector<int> result;
        return result;
    }
};

int main(void)
{
    Solution ss;

    int n[] = {-3, 1, 2, -3, 4};
    vector<int> vn(n, n + sizeof(n) / sizeof(int));
    vector<int> ret;
    ret = ss.subarraySum(vn);

    return 0;
}