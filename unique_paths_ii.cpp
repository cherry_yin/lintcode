#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
	int uniquePathsWithObstacles(vector<vector<int> > &obstacleGrid) {
		if (obstacleGrid.empty() || obstacleGrid[0].empty())
			return 0;

		int m = obstacleGrid.size();
		int n = obstacleGrid[0].size();

		vector<vector<int> > ret(m, vector<int>(n, 0));

		for(int i = 0; i <= n-1; i++) 
		{
			if (obstacleGrid[0][i] == 1)
				break;
			
			ret[0][i] = 1;
		}

		for(int i = 0; i <= m-1; i++)
		{
			if(obstacleGrid[i][0] == 1)
				break;
			
			ret[i][0] = 1;
		}

		for(int i = 1; i <= m-1; i++)
		{
			for(int j = 1; j <= n-1; j++)
			{
				ret[i][j] = (1-obstacleGrid[i-1][j]) * ret[i-1][j] + (1-obstacleGrid[i][j-1]) * ret[i][j-1];
				ret[i][j] = (1-obstacleGrid[i][j]) * ret[i][j];
			}
		}

		return ret[m-1][n-1];
	}
};


int main(void)
{
	vector<vector<int> > grid(3, vector<int>(3,0));
	grid[1][1] = 1;

	Solution s;
	cout << s.uniquePathsWithObstacles(grid) << endl;

	return 0;
}